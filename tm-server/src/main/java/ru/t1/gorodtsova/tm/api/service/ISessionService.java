package ru.t1.gorodtsova.tm.api.service;

import ru.t1.gorodtsova.tm.model.Session;

public interface ISessionService extends IUserOwnerService<Session> {
}
