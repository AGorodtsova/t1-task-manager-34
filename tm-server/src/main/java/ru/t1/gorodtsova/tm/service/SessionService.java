package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.repository.ISessionRepository;
import ru.t1.gorodtsova.tm.api.service.ISessionService;
import ru.t1.gorodtsova.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}
