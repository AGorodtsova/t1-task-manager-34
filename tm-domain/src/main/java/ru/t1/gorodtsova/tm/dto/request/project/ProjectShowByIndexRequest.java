package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIndexRequest;

@NoArgsConstructor
public final class ProjectShowByIndexRequest extends AbstractIndexRequest {

    public ProjectShowByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token, index);
    }

}
