package ru.t1.gorodtsova.tm.dto.request.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIndexRequest;

@NoArgsConstructor
public final class TaskStartByIndexRequest extends AbstractIndexRequest {

    public TaskStartByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token, index);
    }

}
