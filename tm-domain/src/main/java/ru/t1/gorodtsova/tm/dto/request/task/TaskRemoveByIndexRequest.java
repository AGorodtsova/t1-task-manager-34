package ru.t1.gorodtsova.tm.dto.request.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIndexRequest;

@NoArgsConstructor
public final class TaskRemoveByIndexRequest extends AbstractIndexRequest {

    public TaskRemoveByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token, index);
    }

}
