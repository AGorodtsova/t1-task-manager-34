package ru.t1.gorodtsova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Task;

@NoArgsConstructor
public final class TaskStartByIndexResponse extends AbstractTaskResponse {

    public TaskStartByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
